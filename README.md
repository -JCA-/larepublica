## DEPENDENCIAS

Para el correcto funcionamiento del servicio, se requiere de:

1. PostgreSQL 9.6
2. Python 2.7
3. Módulos Python: requests, BeautifulSoup y psycopg2
4. Django 2
5. Django Rest Framework

## CONFIGURANDO BASE DE DATOS EN POSTGRESQL

#Crear la base de datos larepublica

1. psql
2. CREATE DATABASE larepublica;

#Crear el usuario y asignar permisos

1. $ CREATE USER djr_larepublica PASSWORD 'larepublica.dj.py';
2. $ GRANT ALL PRIVILEGES ON DATABASE larepublica TO djr_larepublica;
3. $ GRANT SELECT, DELETE, INSERT, UPDATE ON ALL TABLES IN SCHEMA public TO djr_larepublica;

##CREANDO LAS TABLAS

1. cd ruta/carpeta/larepublica
2. python manage.py makemigrations noticias
3. python manage.py migrate

Nota: En caso ocurra error, comentar la linea #update(urlprincipal) en el archivo noticias/views.py

Archivo:	views.py  > linea 96 >	#update(urlprincipal)

##Ejecutar el servidor

1. cd ruta/carpeta/larepublica 
2. python manage.py runserver
3. abrir la url http://localhost:8000/noticias
