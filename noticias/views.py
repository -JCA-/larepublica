# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from rest_framework import generics 
from noticias.models import *
from noticias.serializers import *

from requests import get
from bs4 import BeautifulSoup
from datetime import datetime, date
import locale

	
def descarga(url):
	"""Descarga la url y actualiza la tabla Noticias"""
	locale.setlocale(locale.LC_ALL, '')
	print "en descarga", url
	response = get(url)
	soup = BeautifulSoup(response.content, 'html.parser')
	[e.extract() for e in soup.findAll("script")]
	
	articulo = soup.find("div", {"class":"internal"})
	if articulo == None:
		print "Se excluye descarga:", url
	else:
		print "Descarga:", len(articulo)
		fechastr = articulo.find("span", {"class":"InternalPublishDate"})
		fecha = datetime.strptime(fechastr.text.split(" | ")[0], "%d %b %Y")	#ejm: 	12 Dic 2019 | 12:12 h
		img = articulo.find("img", {"class":"ImagenInterna"})["srcset"]
		titulo = articulo.find("h1", {"class":"DefaultTitle"}).text
		descripcion = articulo.find("h2", {"class":"DefaultSubtitle"}).text[:250]
		contenido = articulo.find("div", {"class":"page-internal-content"}).text

		if Noticias.objects.filter(url_post=url).count() == 0:
			n = Noticias.objects.create(fecha=fecha, url_post=url, img_source=img, titulo=titulo, descripcion=descripcion, contenido=contenido, modified=fecha, created=date.today())
			#n = Noticias(url_post=url, img_source=img, titulo=titulo, descripcion=descripcion, contenido=contenido)
			n.save()
		else:
			n = Noticias.objects.get(url_post=url)
			if n.modified == None or fecha.date() > n.modified.date():
				n.fecha = fecha
				#n.url_post = url
				n.img_source = img
				n.titulo = titulo
				n.descripcion = descripcion
				n.contenido = contenido
				n.modified = date.today()
				n.save()

def excluido(url):
	#presente = len([True for e in self.urls_excluidos if e in url])
	url = url.lower()
	if url.strip()[0] != "/":
		presente = len([True for e in ["//larepublica.pe/", "//www.larepublica.pe/"] if e in url])
		if presente > 0:
			return False
		else:
			return True
	else:
		return False


def update(url_post):
	response = get(url_post)
	soup = BeautifulSoup(response.content, 'html.parser')
	#contenido total
	div_ctn_home = soup.find("div", {"class":"container container-home"})
	#total de links:
	lst_a = div_ctn_home.find_all("a")
	
	lst_links = []
	lst_hrefs = []
	contador = 0
	for i,a in enumerate(lst_a):
		#contador += 1
		url = a["href"]
		if url.strip()[0]=="/":
			url = url_post+url
		
		if url not in lst_hrefs and not excluido(url) and len(a["href"].split("/"))>3:
			print "\n", i, a["href"]
			
			#lst_links.append(enlace(a=a, url=url))
			descarga(url)
			lst_hrefs.append(url)
	
	print "Links encontrados", len(lst_links)
	#return lst_links, lst_hrefs


# Create your views here.
class NoticiasList(generics.ListCreateAPIView):
	urlprincipal = "https://larepublica.pe/"
	
	update(urlprincipal)
	queryset = Noticias.objects.all()
	serializer_class = NoticiasSerializer