# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from datetime import date

# Create your models here.
class Noticias(models.Model):
	fecha = models.DateTimeField()
	url_post = models.URLField(max_length=300)
	img_source = models.URLField(max_length=300)
	titulo = models.TextField()
	descripcion = models.TextField()
	contenido = models.TextField()
	created = models.DateTimeField(auto_now_add=True)	#default=date.today()
	modified = models.DateTimeField()

