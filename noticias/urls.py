from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from noticias import views

urlpatterns = [
	url(r'^noticias/$', views.NoticiasList.as_view()), 
	]

urlpatterns = format_suffix_patterns(urlpatterns)