from rest_framework import serializers
from noticias.models import *

class NoticiasSerializer(serializers.ModelSerializer):
	class Meta:
		model = Noticias
		fields = ('fecha', 'url_post', 'img_source', 'titulo', 'descripcion', 'contenido', 'modified')
